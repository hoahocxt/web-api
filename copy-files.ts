import * as shell from 'shelljs';

shell.cp('-Rf', 'src/templates/', 'dist/');
shell.cp('-Rf', 'version', 'src/');
shell.cp('-Rf', 'src/version', 'dist/');

shell.mkdir('-p', [`logs`]);
shell.mkdir('-p', [`pm2`]);

// delete temp folder if any
shell.rm('-Rf', 'temp');