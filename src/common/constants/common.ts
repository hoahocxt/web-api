export const ACTIVATE_HASH_SALT = '899ywsrvhps2a4nx';
export const EMAIL_HEADER = '---Confirm your email address---';
export const EMAIL_FOOTER = '***For security reasons this link will expire in 24 hours.';
export const ONE_MONTH_IN_SECONDS = '30d';
export const ACTIVATE_EXPIRE_IN_HOURS = 24;
