import * as fs from 'fs';
import { ReadStream } from 'fs';
import { CommonUtils } from './common.utils';

export class FileUtils {
  public static isExist(filePath: string): boolean {
    return fs.existsSync(filePath);
  }

  public static readFile(filePath: string): string {
    return fs.readFileSync(filePath, 'utf-8');
  }

  public static readFileAsync(filePath: string): Promise<string> {
    return new Promise((resolve, reject) => {
      fs.readFile(filePath, (err, result) => {
        if (err) {
          reject(err);
        } else {
          if (CommonUtils.isNullOrUndefined(result)) {
            reject(`[FileUtils][readFileAsync] File is null ${filePath}`);
          }
          resolve(result.toString('utf8'));
        }
      });
    });
  }

  public static writeFile(filePath: string, content: string) {
    fs.writeFile(filePath, content, 'utf-8', (error) => {
      if (error) {
        return error;
      }
    });
  }

  public static createReadStream(filePath: string): ReadStream {
    return fs.createReadStream(filePath);
  }

  public static unlink(filePath: string): void {
    return fs.unlinkSync(filePath);
  }
}
