
// watch video on pluralsight
import { randomBytes, createHash, createCipher, createDecipher, Cipher } from 'crypto';

export class HashUtils {

    public static hashPassword(password: string, salt: string): string {
        const hash = createHash('sha256').update(password);
        hash.update(salt);
        return hash.digest('hex');
    }

    public static genRandomString(numberOfCharacter: number): string {
        return randomBytes(numberOfCharacter).toString('hex');
    }

    public static encryptKey(key: string, salt: string): string {
        const cipher = createCipher('aes-256-cbc', salt);
        return cipher.update(key, 'utf8', 'hex') + cipher.final('hex');
    }

    public static decryptKey(cipherText: string, salt: string): string {
        const decipher = createDecipher('aes-256-cbc', salt);
        return decipher.update(cipherText, 'hex', 'utf8') + decipher.final('utf8');
    }
}
