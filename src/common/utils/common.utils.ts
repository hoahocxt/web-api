export class CommonUtils {

    public static diffMilliseconds(date1: Date, date2: Date): number {
        const millisecond = date1.getTime() - date2.getTime();
        return millisecond;
    }

    public static diffTotalSeconds(date1: Date, date2: Date): number {
        const millisecond = this.diffMilliseconds(date1, date2);
        return Math.floor(millisecond / 1000);
    }

    public static diffTotalMinutes(date1: Date, date2: Date): number {
        const second = this.diffMilliseconds(date1, date2);
        return Math.floor(second / 60);
    }

    public static isNullOrUndefined(value: any) {
        return typeof value === 'undefined' || value == null;
    }

    public static isNullOrUndefinedOrEmpty(value: any) {
        return typeof value === 'undefined' || value === null || value === '';
    }

    public static isNotNullOrUndefined(value: any) {
        return typeof value !== 'undefined' && value != null;
    }

    public static isNotNullOrUndefinedOrEmpty(value: any) {
        return typeof value !== 'undefined' && value != null && value !== '';
    }
}
