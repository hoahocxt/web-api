import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { IdentifyModule } from '../indentify/indentify.module';

@Module({
    imports: [IdentifyModule],
    controllers: [UserController],
    providers: [],
})
export class GateWayModule { }
