import { Controller, Post, Body, Inject, Put } from '@nestjs/common';
import { RegisterRequest, RegisterResponse, ResponseMessage, ActivateAccountRequest, ActivateAccountResponse } from 'web-common/dist/models/';
import { ApiUseTags } from '@nestjs/swagger';
import { UserService } from '../indentify/user/user.service';
import { DOMAIN_USER } from '../../config';

@ApiUseTags('users')
@Controller('/api/users')
export class UserController {
  constructor(
    @Inject(UserService) public readonly userService: UserService,
  ) { }

  @Post()
  public async registerUser(@Body() registerRequest: RegisterRequest): Promise<RegisterResponse> {
    return this.userService.registerUser(registerRequest, DOMAIN_USER);
  }

  @Put('/activate')
  public async activateUser(@Body() request: ActivateAccountRequest): Promise<ActivateAccountResponse> {
    return await this.userService.activateUser(request);
  }
}
