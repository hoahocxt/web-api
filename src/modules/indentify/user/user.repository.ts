import { Injectable } from '@nestjs/common';
import { User, Role } from 'web-common/dist/models';
import UserEntity from './user.entity';
import RoleEntity from '../role/role.entity';

@Injectable()
export class UserRepository {
    // tslint:disable-next-line:no-empty
    constructor() { }

    public async findByEmail(email: string): Promise<User> {
        const entity = await UserEntity.findOne({ where: { email } });
        if (entity) {
            const user: User = this.toModel(entity);
            return user;
        }
        return null;
    }

    public async findById(id: number): Promise<User> {
        const entity = await UserEntity.findOne({ include: [RoleEntity], where: { id } });
        if (entity) {
            const user: User = this.toModel(entity);
            user.roles = [];
            entity.roles.forEach(role => {
                user.roles.push(new Role({
                    id: role.id,
                    name: role.name,
                }));
            });
            return user;
        }
        return null;
    }

    public async insert(user: User): Promise<User> {
        const entity = this.toEntity(user);
        if (entity) {
            const result = await entity.save();
            return this.toModel(result);
        }
        return null;
    }

    public async update(user: User): Promise<User> {
        const entity = await UserEntity.findOne({ where: { id: user.id } });
        if (entity) {
            this.toEntity(user, entity);
            const result = await entity.save();
            if (result) {
                return this.toModel(result);
            }
            return null;
        }
        return null;
    }

    public toEntity(model: User, entity?: UserEntity): UserEntity {
        if (!entity) {
            entity = new UserEntity();
        }
        entity.id = model.id;
        entity.email = model.email;
        entity.password = model.password;
        entity.name = model.name;
        entity.isEnabledTFA = model.isEnabledTFA;
        entity.isEmailConfirm = model.isEmailConfirm;
        entity.createTime = model.createTime;
        entity.phoneNumber = model.phoneNumber;
        entity.tfaSecret = model.tfaSecret;
        entity.activationCode = model.activationCode;
        entity.salt = model.salt;
        entity.loginFailCount = model.loginFailCount;
        entity.lastLoginFailTime = model.lastLoginFailTime;

        return entity;
    }

    private toModel(entity: UserEntity): User {
        return new User({
            id: entity.id,
            email: entity.email,
            password: entity.password,
            name: entity.name,
            isEnabledTFA: entity.isEnabledTFA,
            isEmailConfirm: entity.isEmailConfirm,
            createTime: entity.createTime,
            lastLoginTime: entity.lastLoginTime,
            phoneNumber: entity.phoneNumber,
            tfaSecret: entity.tfaSecret,
            activationCode: entity.activationCode,
            salt: entity.salt,
            lastLoginFailTime: entity.lastLoginFailTime,
            loginFailCount: entity.loginFailCount,
        });
    }
}
