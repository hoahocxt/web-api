import { Injectable, Inject } from '@nestjs/common';
// tslint:disable-next-line:max-line-length
import { RegisterRequest, RegisterResponse, User, ResponseMessage, ResponseStatus, ActivationCodePayload, ActivateAccountRequest, ActivateAccountResponse, UserRole } from 'web-common/dist/models';
import { UserRepository } from './user.repository';
import { CommonUtils } from '../../../common/utils/common.utils';
import { Resource } from '../../../app.resource';
import { HashUtils } from '../../../common/utils/hash.utils';
import { ACTIVATE_HASH_SALT, ACTIVATE_EXPIRE_IN_HOURS } from '../../../common/constants/common';
import { EmailService } from '../../../modules/shared/email.service';
import { VERIFICATION_MAIL_TEMPLATE_PATH } from '../../../config';
import { UserRoleService } from '../user-role/user-role.service';
// import logger from 'src/common/utils/logger';

@Injectable()
export class UserService {
    constructor(
        @Inject(UserRepository) private readonly userRepository: UserRepository,
        @Inject(EmailService) private readonly emailService: EmailService,
        @Inject(UserRoleService) private readonly userRoleService: UserRoleService,
    ) { }

    public async registerUser(request: RegisterRequest, baseURL: string): Promise<RegisterResponse> {
        try {
            /** tuning request */
            request.email = request.email.toLocaleLowerCase().trim();
            /** verifying captcha */

            /** verify user email is existing */
            const user: User = await this.userRepository.findByEmail(request.email);
            if (CommonUtils.isNotNullOrUndefined(user)) {
                return new RegisterResponse({
                    responseMessage: new ResponseMessage({
                        status: ResponseStatus.Fail,
                        messageCode: 'ACCOUNT_IS_EXISTED_MESSAGE',
                        message: Resource.ACCOUNT_IS_EXISTED_MESSAGE,
                    }),
                });
            }

            /** Create a new user */
            const randCode = HashUtils.genRandomString(30);
            const randSalt = HashUtils.genRandomString(30);
            const hashPassword = HashUtils.hashPassword(request.password, randSalt);

            const newUser = new User({
                email: request.email,
                password: hashPassword,
                name: request.displayName,
                isEmailConfirm: false,
                activationCode: randCode,
                salt: randSalt,
                loginFailCount: 0,
                createTime: new Date(),
            });
            // after use database need to check data first
            const savedUser = await this.userRepository.insert(newUser);
            if (CommonUtils.isNullOrUndefined(savedUser)) {
                return new RegisterResponse({
                    responseMessage: new ResponseMessage({
                        status: ResponseStatus.Fail,
                        messageCode: 'ACCOUNT_INSERT_FAIL_MESSAGE',
                        message: Resource.ACCOUNT_INSERT_FAIL_MESSAGE,
                    }),
                });
            }
            /** adding saved user's role */
            const userRole = new UserRole({
                userId: savedUser.id,
                roleId: Resource.ROLE_ID,
            });

            const savedUserRole = await this.userRoleService.createUserRole(userRole);

            /** send email verification */
            this.sendVerificationEmailToUser(savedUser, baseURL);
            /** save user email to Redis */

            return new RegisterResponse({
                responseMessage: new ResponseMessage({
                    status: ResponseStatus.Success,
                    messageCode: 'ACCOUNT_INSERT_SUCCESS_MESSAGE',
                    message: Resource.ACCOUNT_INSERT_SUCCESS_MESSAGE,
                }),
            });
        } catch (error) {
            return new RegisterResponse({
                responseMessage: new ResponseMessage({
                    status: ResponseStatus.Fail,
                    messageCode: 'COMMON_ERROR',
                    message: Resource.COMMON_ERROR,
                }),
            });
        }
    }

    private async sendVerificationEmailToUser(user: User, baseURL: string) {
        const acticationCodePayload = new ActivationCodePayload({
            userId: user.id,
            code: user.activationCode,
            created: Date.now(),
        });

        const activationCodeEncrypted: string = HashUtils.encryptKey(JSON.stringify(acticationCodePayload), ACTIVATE_HASH_SALT);
        const receiver = user.email;
        const subject = Resource.VERIFICATION_MAIL_SUBJECT;
        const verifyURL = `${baseURL}/account/verify-account?code=${activationCodeEncrypted}`;
        const content = await this.emailService.getMailContentDemo(VERIFICATION_MAIL_TEMPLATE_PATH, verifyURL);
        this.emailService.sendMail(receiver, subject, content);
    }

    /**
     * activate user
     * @param request
     */
    public async activateUser(request: ActivateAccountRequest): Promise<ActivateAccountResponse> {
        try {
            const decryptStr = HashUtils.decryptKey(request.activationCode, ACTIVATE_HASH_SALT);
            const parsedActicateCode: ActivationCodePayload = JSON.parse(decryptStr);

            /** User is not found */
            const userFound: User = await this.userRepository.findById(parsedActicateCode.userId);
            if (!CommonUtils.isNotNullOrUndefined(userFound)) {
                return new ActivateAccountResponse({
                    responseMessage: new ResponseMessage({
                        status: ResponseStatus.Fail,
                        messageCode: 'ACTIVATE_CODE_USER_NOT_FOUND',
                        message: Resource.ACTIVATE_CODE_USER_NOT_FOUND,
                    }),
                });
            }

            /** User is already activated */
            if (userFound.isEmailConfirm === true) {
                return new ActivateAccountResponse({
                    responseMessage: new ResponseMessage({
                        status: ResponseStatus.Fail,
                        messageCode: 'ACTIVATE_CODE_USER_WAS_ACTIVATED',
                        message: Resource.ACTIVATE_CODE_USER_WAS_ACTIVATED,
                    }),
                    email: userFound.email,
                });
            }

            /** activation code is invalid or expired */
            const expiredInSeconds = CommonUtils.diffMilliseconds(new Date(), new Date(parsedActicateCode.created));

            // 1 hour = 3600 seconds
            // 1 seconds = 1000 milliseconds
            const validInSeconds = ACTIVATE_EXPIRE_IN_HOURS * 3600 * 1000;
            if (parsedActicateCode.code !== userFound.activationCode || expiredInSeconds > validInSeconds) {
                return new ActivateAccountResponse({
                    responseMessage: new ResponseMessage({
                        status: ResponseStatus.Fail,
                        messageCode: 'ACTIVATE_CODE_IS_INVALID',
                        message: Resource.ACTIVATE_CODE_IS_INVALID,
                    }),
                });
            }

            userFound.isEmailConfirm = true;
            userFound.activationCode = '';

            const userSaved = await this.userRepository.update(userFound);

            if (!CommonUtils.isNotNullOrUndefined(userSaved)) {
                return new ActivateAccountResponse({
                    responseMessage: new ResponseMessage({
                        status: ResponseStatus.Fail,
                        messageCode: 'COMMON_ERROR',
                        message: Resource.COMMON_ERROR,
                    }),
                });
            }

            return new ActivateAccountResponse({
                responseMessage: new ResponseMessage({
                    status: ResponseStatus.Success,
                    messageCode: 'ACTIVATE_USER_ACCOUNT_SUCCESS',
                    message: Resource.ACTIVATE_USER_ACCOUNT_SUCCESS,
                }),
                email: userFound.email,
            });
        } catch (error) {
           // logger.error(error);
            return new ActivateAccountResponse({
                responseMessage: new ResponseMessage({
                    status: ResponseStatus.Fail,
                    messageCode: 'COMMON_ERROR',
                    message: Resource.COMMON_ERROR,
                }),
            });
        }
    }
}
