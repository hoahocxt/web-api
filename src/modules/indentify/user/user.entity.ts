import { Table, Model, Unique, AutoIncrement, AllowNull, IsNumeric, Column, BelongsToMany, HasMany } from 'sequelize-typescript';
import RoleEntity from '../role/role.entity';
import UserRoleEntity from '../user-role/user-role.entity';

@Table({ tableName: 'user' })
export default class UserEntity extends Model<UserEntity> {
    @Unique
    @AutoIncrement
    @AllowNull(false)
    @IsNumeric
    @Column({ primaryKey: true })
    public id: number;

    @AllowNull(false)
    @Unique
    @Column({ field: 'email' })
    public email: string;

    @AllowNull(false)
    @Column({ field: 'password-hash' })
    public password: string;

    @Column({ field: 'full_name' })
    public name: string;

    @Column({ field: 'security_stamp' })
    public securityStamp: string;

    @Column({ field: 'is_email_confirm' })
    public isEmailConfirm: boolean;

    @Column({ field: 'create_time' })
    public createTime: Date;

    @Column({ field: 'last_login_time' })
    public lastLoginTime: Date;

    @Column({ field: 'phone_number' })
    public phoneNumber: string;

    @Column({ field: 'tfa_secret' })
    public tfaSecret: string;

    @Column({ field: 'is_enable_tfa' })
    public isEnabledTFA: boolean;

    @Column({ field: 'activation_code' })
    public activationCode: string;

    @Column({ field: 'salt' })
    public salt: string;

    @Column({ field: 'last_login_fail_time' })
    public lastLoginFailTime: Date;

    @Column({ field: 'login_fail_count' })
    public loginFailCount: number;

    @BelongsToMany(() => RoleEntity, () => UserRoleEntity )
    public roles: RoleEntity[];

    @HasMany(() => UserRoleEntity)
    public userRoles: UserRoleEntity[];

}
