import { Module } from '@nestjs/common';
import { UserRepository } from './user/user.repository';
import { UserService } from './user/user.service';
import { EmailService } from '../shared/email.service';
import { SharedLibsModule } from '../shared/shared-libs.module';
import { AuthModule } from './auth/auth.module';
import { RoleModule } from './role/role.module';
import { UserRoleModule } from './user-role/user-role.module';

@Module({
  controllers: [],
  imports: [SharedLibsModule, AuthModule, RoleModule, UserRoleModule],
  providers: [
    UserRepository,
    UserService,
    EmailService,
  ],
  exports: [
    UserService,
    EmailService,
    SharedLibsModule,
  ],
})
export class IdentifyModule { }
