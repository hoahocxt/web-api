import { Table, Unique, AllowNull, AutoIncrement, Column, ForeignKey, BelongsTo, Model } from 'sequelize-typescript';
import UserEntity from '../user/user.entity';
import RoleEntity from '../role/role.entity';

@Table({tableName: 'user_role'})
export default class UserRoleEntity extends Model<UserRoleEntity> {
  @Unique
  @AllowNull(false)
  @AutoIncrement
  @Column({primaryKey: true})
  public id: number;

  @ForeignKey(() => UserEntity)
  @Column({field: 'user_id'})
  public userId: number;

  @BelongsTo(() => UserEntity)
  public user: UserEntity;

  @ForeignKey(() => RoleEntity)
  @Column({ field: 'role_id'})
  public roleId: number;

  @BelongsTo(() => RoleEntity)
  public role: RoleEntity;
}
