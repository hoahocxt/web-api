import { Injectable, Inject } from '@nestjs/common';
import { UserRoleRepository } from './user-role.repository';
import { UserRole } from 'web-common/dist/models';

@Injectable()
export class UserRoleService {
  constructor(
    @Inject(UserRoleRepository) private readonly userRoleRepository: UserRoleRepository,
  ) {}

  public async createUserRole(userRole: UserRole): Promise<UserRole> {
    return this.userRoleRepository.createUserRole(userRole);
  }
}
