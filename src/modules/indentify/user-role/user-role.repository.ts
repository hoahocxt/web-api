import { Injectable } from '@nestjs/common';
import { UserRole } from 'web-common/dist/models';
import UserRoleEntity from './user-role.entity';

@Injectable()
export class UserRoleRepository {
  // tslint:disable-next-line:no-empty
  constructor() { }

  public async createUserRole(userRole: UserRole): Promise<UserRole> {
    const userRoleEntity = this.toEntity(userRole);
    if (userRoleEntity) {
      const savedUserRole = await userRoleEntity.save();
      return this.toModel(savedUserRole);
    }
    return null;
  }

  public toModel(entity: UserRoleEntity): UserRole {
    return new UserRole({
      id: entity.id,
      userId: entity.userId,
      roleId: entity.roleId,
    });
  }

  public toEntity(userRole: UserRole, entity?: UserRoleEntity): UserRoleEntity {
    return new UserRoleEntity({
      id: userRole.id,
      userId: userRole.userId,
      roleId: userRole.roleId,
    });
  }
}
