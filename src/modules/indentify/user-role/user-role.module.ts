import { Module } from '@nestjs/common';
import { UserRoleService } from './user-role.service';
import { UserRoleRepository } from './user-role.repository';

@Module({
  providers: [UserRoleService, UserRoleRepository],
  exports: [UserRoleService, UserRoleRepository],
})
export class UserRoleModule { }
