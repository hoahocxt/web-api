
import { Table, Model, Unique, AllowNull, AutoIncrement, Column, DataType, HasMany } from 'sequelize-typescript';
import { RoleType } from 'web-common/dist/constant';
import UserRoleEntity from '../user-role/user-role.entity';

@Table({ tableName: 'role' })
export default class RoleEntity extends Model<RoleEntity> {
  @Unique
  @AllowNull(false)
  @AutoIncrement
  @Column({ primaryKey: true })
  public id: number;

  @AllowNull(false)
  @Column(DataType.ENUM(RoleType.ADMIN, RoleType.USER, RoleType.OPS))
  public name: RoleType;

  @HasMany(() => UserRoleEntity)
  public userRoles: UserRoleEntity[];

}
