import { EmailService } from './email.service';
import { CacheService } from './cache.service';
import { Module, HttpModule } from '@nestjs/common';

const services = [
  EmailService,
  CacheService,
];
@Module({
  imports: [
    HttpModule,
  ],
  providers: [
    ...services,
  ],
  exports: [
    ...services,
  ],
})
export class SharedLibsModule { }
