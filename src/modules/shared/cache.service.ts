import { Injectable } from '@nestjs/common';

import * as memoryCache from 'memory-cache';

@Injectable()
export class CacheService {
  // tslint:disable-next-line:no-empty
  constructor() {}

  // tslint:disable-next-line:max-line-length
  public async getOrSet<T>(key: string, method: (...args: any[]) => Promise<T>, durationInSecond: number = 120, timeoutCallback?: (...args: any[]) => void): Promise<T> {
    if (key == null || key === undefined || key === '') {
      throw new Error('Cache key is require');
    }

    const valueFromCache = (this.get(key)) as T;
    if (valueFromCache == null || valueFromCache === undefined) {
      const value = await method();
      this.set(key, value, durationInSecond, timeoutCallback);
      return value;
    }

    return valueFromCache;
  }

  public set(key: string, value: any, durationInSecond: number, timeoutCallback?: (...args: any[]) => void) {
    if (key == null || key === undefined || key === '') {
      throw new Error('Cache key is require');
    }

    memoryCache.put(key, value, durationInSecond * 1000, timeoutCallback);
  }

  public get<T>(key: string): T {
    if (key == null || key === undefined || key === '') {
      throw new Error('Cache key is require');
    }

    return (memoryCache.get(key)) as T;
  }
}
