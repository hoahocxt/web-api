import { Injectable, Inject } from '@nestjs/common';
import * as path from 'path';
// tslint:disable-next-line:max-line-length
import { APP_ROOT_PATH, BUNDLE_FOLDER, EMAIL_BODY_TYPE, EMAIl_USER_NAME, EMAIL_CLIENT_ID, EMAIL_CLIENT_SERCRET, EMAIL_REFRESH_TOKEN, EMAIL_FROM, EMAIL_ACCESS_TOKEN, EMAIL_SMPT_HOST, EMAIL_SMPT_PORT } from '../../config';
import { ONE_MONTH_IN_SECONDS, ACTIVATE_EXPIRE_IN_HOURS } from '../../common/constants/common';
import * as nodemailer from 'nodemailer';
import { FileUtils } from '../../common/utils/file.utils';
import { CacheService } from './cache.service';
import * as ms from 'ms';

@Injectable()
export class EmailService {

  constructor(
    @Inject(CacheService) private readonly cacheService: CacheService,
  ) { }

  private getMailTemplatePath(mailTemplatePath: string): string {
    return path.join(`${APP_ROOT_PATH}/${BUNDLE_FOLDER}`, `${mailTemplatePath}.${EMAIL_BODY_TYPE}`);
  }

  public async getMailContentDemo(mailTemplatePath: string, verifyUrl: string): Promise<string> {
    const filePath = this.getMailTemplatePath(mailTemplatePath);
    const cacheKey = `cache-emailTemplate-${mailTemplatePath}`;
    let content = await this.cacheService.getOrSet<string>(cacheKey, () => FileUtils.readFileAsync(filePath), ms(ONE_MONTH_IN_SECONDS) / 1000);
    content = content.replace(/{verifyUrl}/g, verifyUrl);
    content = content.replace(/{EXPIRE_TIME}/g, ACTIVATE_EXPIRE_IN_HOURS.toString());
    return content;
  }

  // private getMailTransporter() {
  //   return nodemailer.createTransport({
  //     host: EMAIL_SMTP_HOST,
  //     port: EMAIL_SMPT_PORT,
  //     auth: {
  //       user: EMAIL_USER_NAME,
  //       pass: EMAIL_PASSWORD,
  //     },
  //     tls: {
  //       rejectUnauthorized: false
  //     }
  //   });
  // }

  // private getMailTransport() {
  //   return nodemailer.createTransport({
  //     service: 'gmail',
  //     auth: {
  //       xoauth2: xoauth2.createXOAuth2Generator({
  //         user: EMAIl_USER_NAME,
  //         clientId: EMAIL_CLIENT_ID,
  //         clientSecret: EMAIL_CLIENT_SERCRET,
  //         refreshToken: EMAIL_REFRESH_TOKEN,
  //       })
  //     }
  //   });
  // }

  private getMailTransport() {
    return nodemailer.createTransport({
      host: EMAIL_SMPT_HOST,
      port: EMAIL_SMPT_PORT,
      secure: true,
      auth: {
        type: 'OAuth2',
        user: EMAIl_USER_NAME,
        clientId: EMAIL_CLIENT_ID,
        clientSecret: EMAIL_CLIENT_SERCRET,
        refreshToken: EMAIL_REFRESH_TOKEN,
        accessToken: EMAIL_ACCESS_TOKEN,
        expires: 1484314697598,
      },
    });
  }

  // public async getMailContent(verify: string): Promise<string> {
  //   const header = EMAIL_HEADER;
  //   const footer = EMAIL_FOOTER;
  //   const content = header + '/n' + verify + '/n' + footer;
  //   return content;
  // }

  public async sendMail(mailReceiver: string, mailSubject: string, content: string): Promise<void> {
    const mailOptions = {
      from: EMAIL_FROM,
      to: mailReceiver,
      subject: mailSubject,
      text: content,
      html: null,
    };

    return new Promise<void>(async (resolve, reject) => {
      this.getMailTransport().sendMail(mailOptions, (error, info) => {
        if (error) {
          throw error;
        } else {
          resolve();
        }
      });
    });
  }
}
