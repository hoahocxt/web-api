import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './config/db.config.module';
import { GateWayModule } from './modules/gateway/gateway.module';
import { IdentifyModule } from './modules/indentify/indentify.module';
import { SharedAppModule } from 'web-common/dist/app';
import { SharedLibsModule } from './modules/shared/shared-libs.module';

@Module({
  imports: [DatabaseModule, GateWayModule, IdentifyModule, SharedAppModule, SharedLibsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {

}
