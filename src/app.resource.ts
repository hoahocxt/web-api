export class Resource {

    public static readonly COMMON_ERROR = 'Error occurred';

    public static readonly ACCOUNT_IS_EXISTED_MESSAGE = 'Email already existed';
    public static readonly ACCOUNT_INSERT_FAIL_MESSAGE = 'Insert account with an error';
    public static readonly ACCOUNT_INSERT_SUCCESS_MESSAGE = 'Please check your email to complete registration';

    public static readonly VERIFICATION_MAIL_SUBJECT = '[Web Project] Please verify your email address';

    public static readonly ACTIVATE_CODE_USER_NOT_FOUND = 'User is not found';
    public static readonly ACTIVATE_USER_ACCOUNT_SUCCESS = 'Activate user account success';
    public static readonly ACTIVATE_CODE_USER_WAS_ACTIVATED = 'Activate code user was activated';
    public static readonly ACTIVATE_CODE_IS_INVALID = 'Activate code is invalid';

    /** Role id is End-user */
    public static readonly ROLE_ID = 2;
}
