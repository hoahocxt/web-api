import * as dotenv from 'dotenv';

export const NODE_ENV = process.env.NODE_ENV || 'dev';
dotenv.config({ path: `${NODE_ENV}.env`});

// tslint:disable-next-line:no-var-requires
export const APP_ROOT_PATH = require('app-root-path');
export const BUNDLE_FOLDER = 'dist';
export const DOMAIN_USER = process.env.DOMAIN_USER;

// database
export const DB_SYNC = (process.env.DB_SYNC === 'true');
export const DB_MYSQL_DATABASE = process.env.DB_MYSQL_DATABASE;
export const DB_MYSQL_USERNAME = process.env.DB_MYSQL_USERNAME;
export const DB_MYSQL_PASSWORD = process.env.DB_MYSQL_PASSWORD;
export const DB_MYSQL_HOST = process.env.DB_MYSQL_HOST;
export const DB_MYSQL_PORT = +(process.env.DB_MYSQL_PORT);

// email
export const EMAIL_BODY_TYPE = process.env.EMAIL_BODY_TYPE;
export const EMAIL_FROM = process.env.EMAIL_FROM;
export const EMAIl_USER_NAME = process.env.EMAIl_USER_NAME;
export const EMAIL_CLIENT_ID = process.env.EMAIL_CLIENT_ID;
export const EMAIL_CLIENT_SERCRET = process.env.EMAIL_CLIENT_SERCRET;
export const EMAIL_REFRESH_TOKEN = process.env.EMAIL_REFRESH_TOKEN;
export const EMAIL_ACCESS_TOKEN = process.env.EMAIL_ACCESS_TOKEN;
export const EMAIL_SMPT_HOST = process.env.EMAIL_SMPT_HOST;
export const EMAIL_SMPT_PORT = process.env.EMAIL_SMPT_PORT;

// email template path
export const VERIFICATION_MAIL_TEMPLATE_PATH = 'templates/verification/verification.email';
