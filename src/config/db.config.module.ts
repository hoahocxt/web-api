import { Module } from '@nestjs/common';
import { Sequelize } from 'sequelize-typescript';
import { DB_MYSQL_HOST, DB_MYSQL_PORT, DB_MYSQL_USERNAME, DB_MYSQL_PASSWORD, DB_MYSQL_DATABASE, DB_SYNC } from './environment';

export const databaseProviders = [
    {
        provide: 'Sequelize',
        useFactory: async () => {
            const sequelize = new Sequelize({
                dialect: 'mysql',
                host: DB_MYSQL_HOST,
                port: DB_MYSQL_PORT,
                username: DB_MYSQL_USERNAME,
                password: DB_MYSQL_PASSWORD,
                database: DB_MYSQL_DATABASE,
                modelPaths: [__dirname + '/../modules/**/*.entity.[tj]s'],
            });
            if (!DB_SYNC) {
                await sequelize.sync();
            }
            return sequelize;
        },
    },
];

@Module({
    providers: [...databaseProviders],
    exports: [...databaseProviders],
})
export class DatabaseModule {}
