import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

import { useContainer } from 'class-validator';

import { SharedAppModule } from 'web-common/dist/app';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Web')
    .setDescription('Study NestJS')
    .setVersion('1.0')
    .setBasePath('/')
    .addBearerAuth('Authorization', 'header', 'apiKey')
    .setSchemes('http', 'https')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-docs', app, document);

  // this is solution to deal with class validator problems.
  const commonValidationPipe = app.select(SharedAppModule).get('CommonValidationPipe');
  useContainer(app.select(AppModule), {fallback: true,  fallbackOnErrors: true });
  app.useGlobalPipes(commonValidationPipe, new ValidationPipe());

  await app.listen(8000);
}
bootstrap();
