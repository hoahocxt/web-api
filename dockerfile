FROM node:8.11.2

WORKDIR /app

COPY ["package.json", "package-lock.json", "./"]
RUN npm install -f && npm run web-common

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]
